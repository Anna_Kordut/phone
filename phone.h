#pragma once
#include "accumulator.h"
#include "camera.h"
#include "dicplay.h"


class Phone {
private:
	Accumulator accumulator;
	dicplay displa;
	camera camers;
public:
	Accumulator get_accumulator();
	dicplay get_display();
	camera get_camera();
	void set_accumulator(Accumulator new_accumulator);
	void set_display(dicplay new_display);
	void set_camera(camera new_camera);
};
