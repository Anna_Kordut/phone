#pragma once

class Accumulator {
private:
	int capacity_accumulator;

public:
	int get_capacity_accumulator();
	void set_capacity_accumulator(int new_capacity_accumulator);


};

