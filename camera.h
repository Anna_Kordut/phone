#pragma once

class camera {
private:
	int number_megapixels;

public:
	int get_number_megapixels();
	
	void set_number_megapixels(int new_number_megapixels);

};
